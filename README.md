# AMiGrafanaInfluxDB


In this tutorial, I will show you how to create a Grafana dashboard to measure a response time of a URL web based scenario check. I assume that you are familiar with Grafana, WebHook and InfluxDB. Anyway these tools are very simple to use. While AMiCli is a custom dev I made based on selenium engine, I won&#39;t discuss much about Grafana, WebHook and InfluxDB.

The idea is to get a response time dashboard on Grafana related to web scenarii defined in AMiCli.

 ![alt text](img/Picture1.png)

The architecture involves:

- Grafana server
- InfluxDB server
- AMi client program
- WebHook (optional)

 ![alt text](img/Picture2.png)
 
To make it simple, a standalone single server will host all the above application (Grafana, InfluxDB, AMi cli, and WebHook). Yet a distributed architecture could be used.

You can download the latest version of the products at the below URLs:

Grafana: [https://grafana.com/grafana/download](https://grafana.com/grafana/download)

InfluxDB: [https://portal.influxdata.com/downloads](https://portal.influxdata.com/downloads) (InfluxDB)

AMi Client : [https://gitlab.com/vi7Ch/AMiCli2InfluxDB\_pub/](https://gitlab.com/vi7Ch/AMiCli2InfluxDB_pub/)

WebHook : [https://github.com/adnanh/webhook](https://github.com/adnanh/webhook) (I&#39;m using this webhook)



**AMi**

 ![alt text](img/Picture3.png)
 
 ![alt text](img/Picture4.png)
 
 ![alt text](img/Picture5.png)
 
 ![alt text](img/Picture6.png)
 
 ![alt text](img/Picture7.png)
 
 ![alt text](img/Picture8.png)
 
 ![alt text](img/Picture9.png)
 
 ![alt text](img/Picture10.png)
 
Let&#39;s create a scheduled task to run the little program AMi that perform the URL based check:

 ![alt text](img/Picture11.png)
 
Define the Action to start in the specific directory where you have the AMiCli binary.

Add the arguments : -XX:-HeapDumpOnOutOfMemoryError -XX:-CreateMinidumpOnCrash -jar AMi2InfluxDB.jar

These will prevent the creation of unnecessary headdump file.

 ![alt text](img/Picture12.png)
 
The AMi2InfluxDB program sits in the below directory

 ![alt text](img/Picture13.png)
 
This folder contains 3 folders, the jar executable, the global config file config.cfg. The phantomjsdriver.log file will be created at runtime. The latter contains the phantomjsdriver log activities. Phantomjs is used as a headless browser; we basically use it to simulate user interaction with the web page.





The logs dir contains the global logfile. During runtime AMi2InfluxDB program will log its activities in this logfile:

```javascript
30-08-2018 - 14:56:14.785 [SERVERDEV] [main] \_[34mINFO \_[0;39m AMi2InfluxDB - Spent 1 minutes, 22.180 seconds

30-08-2018 - 15:04:52.676 [SERVERDEV] [main] \_[34mINFO \_[0;39m AMi2InfluxDB - === Initialization AMi2InfluxDB: Version: 0.1 / Author: Viseth CHAING (Vi7) / Built-Date: Wed Jul 04 23:45:57 CEST 2018

30-08-2018 - 15:06:17.762 [SERVERDEV] [main] \_[34mINFO \_[0;39m AMi2InfluxDB - Spent 1 minutes, 25.224 seconds
```


The exec dir contains the actual binaries of the program

The AMiCLi2InfluxDB.jar file itself:

The program I&#39;ve developed to run Selenuim scenarii and generate screenshot on error.

The chromedriver.exe file:

WebDriver is an open source tool for automated testing of webapps across many browsers. It provides capabilities for navigating to web pages, user input, JavaScript execution, and more.  ChromeDriver is a standalone server which implements WebDriver&#39;s wire protocol for Chromium.

The phantomjs.exe file:

PhantomJS  is a headless web browser.  We basically use it to simulate user interaction with the web page.

 ![alt text](img/Picture14.png)
 
The data dir contains the URL based check monitor folder

 ![alt text](img/Picture15.png)
 
Each folder contains 2 folders and a config file

 ![alt text](img/Picture16.png)
 
- The logs dir contain the specific scenario logfile

 ![alt text](img/Picture17.png)
 
 ```javascript
AMiCli2InfluxDB - === Initialization AMiCli2InfluxDB: Version: 0.1 / Author: Viseth CHAING (Vi7) / Built-Date: Wed Jul 04 16:48:36 CEST 2018

30-08-2018 - 15:05:05.451 [SERVERAMICLI] [main] \_[34mINFO \_[0;39m AMiCli2InfluxDB - --- trying to connect to influxDB aAMi

30-08-2018 - 15:05:16.418 [SERVERAMICLI] [main] \_[1;31mERROR\_[0;39m AMiCli2InfluxDB - Error while navigating though the steps: {&quot;errorMessage&quot;:&quot;Unable to find element with id &#39;username&#39;&quot;,&quot;request&quot;:{&quot;headers&quot;:{&quot;Accept-Encoding&quot;:&quot;gzip&quot;,&quot;Connection&quot;:&quot;Keep-Alive&quot;,&quot;Content-Length&quot;:&quot;33&quot;,&quot;Content-Type&quot;:&quot;application/json; charset=utf-8&quot;,&quot;Host&quot;:&quot;localhost:37020&quot;,&quot;User-Agent&quot;:&quot;okhttp/3.10.0&quot;},&quot;httpVersion&quot;:&quot;1.1&quot;,&quot;method&quot;:&quot;POST&quot;,&quot;post&quot;:&quot;{\&quot;using\&quot;:\&quot;id\&quot;,\&quot;value\&quot;:\&quot;username\&quot;}&quot;,&quot;url&quot;:&quot;/element&quot;,&quot;urlParsed&quot;:{&quot;anchor&quot;:&quot;&quot;,&quot;query&quot;:&quot;&quot;,&quot;file&quot;:&quot;element&quot;,&quot;directory&quot;:&quot;/&quot;,&quot;path&quot;:&quot;/element&quot;,&quot;relative&quot;:&quot;/element&quot;,&quot;port&quot;:&quot;&quot;,&quot;host&quot;:&quot;&quot;,&quot;password&quot;:&quot;&quot;,&quot;user&quot;:&quot;&quot;,&quot;userInfo&quot;:&quot;&quot;,&quot;authority&quot;:&quot;&quot;,&quot;protocol&quot;:&quot;&quot;,&quot;source&quot;:&quot;/element&quot;,&quot;queryKey&quot;:{},&quot;chunks&quot;:[&quot;element&quot;]},&quot;urlOriginal&quot;:&quot;/session/53fb83f0-ac55-11e8-a3ee-f5588c21ffec/element&quot;}}

Command duration or timeout: 0 milliseconds

30-08-2018 - 15:05:18.120 [SERVERAMICLI] [main] \_[34mINFO \_[0;39m AMiCli2InfluxDB - --- database influxDB: responseTime -\&gt; 1535634318119, NNMi-PPRD-2, FAILED, 0

30-08-2018 - 15:05:18.177 [SERVERAMICLI] [main] \_[34mINFO \_[0;39m AMiCli2InfluxDB - sending point to influxdb responseTime succeeded

30-08-2018 - 15:15:04.321 [SERVERAMICLI] [main] \_[34mINFO \_[0;39m AMiCli2InfluxDB - === Initialization AMiCli2InfluxDB: Version: 0.1 / Author: Viseth CHAING (Vi7) / Built-Date: Wed Jul 04 16:48:36 CEST 2018

30-08-2018 - 15:15:06.518 [SERVERAMICLI] [main] \_[34mINFO \_[0;39m AMiCli2InfluxDB - --- trying to connect to influxDB aAMi

30-08-2018 - 15:15:16.588 [SERVERAMICLI] [main] \_[1;31mERROR\_[0;39m AMiCli2InfluxDB - Error while navigating though the steps: {&quot;errorMessage&quot;:&quot;Unable to find element with id &#39;username&#39;&quot;,&quot;request&quot;:{&quot;headers&quot;:{&quot;Accept-Encoding&quot;:&quot;gzip&quot;,&quot;Connection&quot;:&quot;Keep-Alive&quot;,&quot;Content-Length&quot;:&quot;33&quot;,&quot;Content-Type&quot;:&quot;application/json; charset=utf-8&quot;,&quot;Host&quot;:&quot;localhost:12172&quot;,&quot;User-Agent&quot;:&quot;okhttp/3.10.0&quot;},&quot;httpVersion&quot;:&quot;1.1&quot;,&quot;method&quot;:&quot;POST&quot;,&quot;post&quot;:&quot;{\&quot;using\&quot;:\&quot;id\&quot;,\&quot;value\&quot;:\&quot;username\&quot;}&quot;,&quot;url&quot;:&quot;/element&quot;,&quot;urlParsed&quot;:{&quot;anchor&quot;:&quot;&quot;,&quot;query&quot;:&quot;&quot;,&quot;file&quot;:&quot;element&quot;,&quot;directory&quot;:&quot;/&quot;,&quot;path&quot;:&quot;/element&quot;,&quot;relative&quot;:&quot;/element&quot;,&quot;port&quot;:&quot;&quot;,&quot;host&quot;:&quot;&quot;,&quot;password&quot;:&quot;&quot;,&quot;user&quot;:&quot;&quot;,&quot;userInfo&quot;:&quot;&quot;,&quot;authority&quot;:&quot;&quot;,&quot;protocol&quot;:&quot;&quot;,&quot;source&quot;:&quot;/element&quot;,&quot;queryKey&quot;:{},&quot;chunks&quot;:[&quot;element&quot;]},&quot;urlOriginal&quot;:&quot;/session/ba5ca9c0-ac56-11e8-956e-e3d1f297e485/element&quot;}}

Command duration or timeout: 0 milliseconds

30-08-2018 - 15:15:18.014 [SERVERAMICLI] [main] \_[34mINFO \_[0;39m AMiCli2InfluxDB - --- database influxDB: responseTime -\&gt; 1535634918010, NNMi-PPRD-2, FAILED, 0

30-08-2018 - 15:15:18.045 [SERVERAMICLI] [main] \_[34mINFO \_[0;39m AMiCli2InfluxDB - sending point to influxdb responseTime succeeded
```


That logfile tells you the status of that URL based check.

This status along with the response time will be sent to the influxDB, that will later be graphed by Grafana.

- The config file

Here&#39;s what a config file would look like. It&#39;s basically a Selenuim scenario. The language to use is java or groovy :

```javascript
mmi.URLSteps='''

driver.get("https://serverWebTest.test.com/nnm/main")

if(driver.findElement(By.id('username'))) {

        driver.findElement(By.id('username')).sendKeys('admin')

}

if(driver.findElement(By.id("password"))) {

        driver.findElement(By.id("password")).clear()

}

if(driver.findElement(By.id("password"))) {

        driver.findElement(By.id("password")).sendKeys(MMiDecrypt('0/PkO7WgtfpXJ+M9dwQ/Vg=='))

}

Thread.sleep(1000)

if(driver.findElement(By.id("login-form\_\_submit"))) {

        driver.findElement(By.id("login-form\_\_submit")).click()

}

Thread.sleep(5000)

if(driver.findElement(By.id("IncidentMgmt\_button\_title"))) {

        driver.findElement(By.id("IncidentMgmt\_button\_title")).click()

}

'''
```


- The ScreenShots dir contains the last screenshot on error if any along with the last 10 screenShot on error.

 ![alt text](img/Picture18.png)
 
The filename of the screenshotOnError file contains the date and time it was logged.
